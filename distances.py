#!/usr/bin/env	python

# std
import sys
# my
from classes import *

# misc
from mongoengine import *

def main():
	connect("test")

	dotsOne = Dot.objects
	dotsTwo = Dot.objects
	f = open("/tmp/distances", "w")

	for i in dotsOne:
		for j in dotsTwo:
			f.write("%.2f\n" % get_distance_between(i, j))
	f.close()
	f = open("/tmp/distances", "r")

	d = {}
	for line in f:
		dist = float(line)
		if dist in d:
			d[dist] += 1
		else:
			d[dist] = 1

	d[0] -= Dot.objects.count()
	for key, val in d.iteritems():
		print "%s,%s" % (key, val >> 1)
	

	return 0

if __name__ == "__main__":
	exit(main())
