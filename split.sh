#!/usr/bin/env  bash

DSTDIR=$2
PCAPFILE=$1
DIR=all

mkdir $DSTDIR/$DIR


for i in {0..30000}
do
    FILEPATH=$DSTDIR/$DIR/tcpstream_$i.pcap
    tshark -nr $PCAPFILE -Y "tcp.stream eq $i" -w $FILEPATH
    # tcpdump -r $FILEPATH -w $FILEPATH
    FILESIZE=$(stat -c%s "$FILEPATH")
    if [ $FILESIZE -eq 268 ]
    	then
	    	rm $FILEPATH
	    	break
    fi
done

for i in {0..30000}
do
    FILEPATH=$DSTDIR/$DIR/tcpstream_$i
    tcpdump -r $FILEPATH.pcap -w $FILEPATH.cap
done
