# std libs
import sys
import math

# misc
from scapy.all import *
from mongoengine import *

class Cluster(Document):
        '''optics=1, k-means=2'''
        alg_type = IntField()


        def center_dot(self):
            dots = self.dots
            for dot in dots[:]:
                if dot.is_center:
                    return dot
            return None

        def make_center_dot(self):
            dots = Dot.objects(cluser=self)
            for dot in dots[:]:
                


        def show(self):
            print '----'+str(self.id)+'----'
            dots = Dot.objects(cluster=self)
            for dot in dots:
                print dot.__str__()
            print '------' + '-' * len(str(self.id)) + '------'



        def __repr__(self):
            return "<Cluster(id='%s', alg_type='%s'>" % \
                (str(self.id), str(self.alg_type))


        def __str__(self):
            return "<Cluster(id='%s', alg_type='%s'>" % \
                (str(self.id), str(self.alg_type))


class Dot(Document):
        session = StringField()
        cluster = ReferenceField(Cluster)
        is_center = BooleanField(default=False)
        output_num = FloatField()
        packet_count = FloatField()
        is_used = BooleanField(default=False)
        reachability_distance = FloatField(default=-1)
        coredistance = FloatField(default=-1)


        def __repr__(self):
            format = "<Dot(id='%s', session='%s', cluster='%s', output_num='%s', packet_count='%s')>"
            return format % (str(self.id), self.session, self.cluster, self.output_num, self.packet_count)

        def __str__(self):
            format = "<Dot(id='%s', session='%s', cluster='%s', output_num='%s', packet_count='%s')>"
            return format % (str(self.id), self.session, self.cluster, self.output_num, self.packet_count)



        def get_attrs_for_compare(self):
            attrs_dict = {'output_num':self.output_num, 'packet_count':self.packet_count}
            return attrs_dict

        def getNeighborsAndMinD(self, ep, dots):
            neighbors = {}
            for dot in dots[:]:
                d = get_distance_between(self, dot)
                if d <= ep:
                    neighbors[dot] = d
            return neighbors

        def get_neighbors(self, r, dots):
            neighbors = []
            for dot in dots[:]:
                d = get_distance_between(self, dot)
                if d <= r:
                    neighbors.append(dot)
            
            return neighbors


        def is_in_cluster(self, cluster, ep):
            attrs_dict1 = self.get_attrs_for_compare()
            s = 0
            for key, value in attrs_dict1.iteritems():
                  if isinstance(value, int) or isinstance(value, int):
                      summand = (value - getattr(cluster, key))**2
                  elif isinstance(value,str) or isinstance(value, unicode):
                      cluster_attr = getattr(cluster, key)
                      set_for_summand = set(value) - set(cluster_attr) if len(value) > len(cluster_attr) else set(cluster_attr) - set(value)
                      summand = len(set_for_summand) ** 2
                  s += summand
            if math.sqrt(s) <= ep:
                return True
            return False


def get_distance_between(dot1, dot2):
    """Euclidean distance"""
    attrs_dict1 = dot1.get_attrs_for_compare()
    s = 0
    for key, value in attrs_dict1.iteritems():
          if isinstance(value, int) or isinstance(value, float):
              summand = (value - getattr(dot2, key))**2
          elif isinstance(value,str) or isinstance(value, unicode):
              dot2_attr = getattr(dot2, key)
              set_for_summand = set(value) - set(dot2_attr) if len(value) > len(dot2_attr) else set(dot2_attr) - set(value)
              summand = len(set_for_summand) ** 2
          s += summand
    return math.sqrt(s)


def set_dots_not_used(dots):
    dots.update(set__is_used=False)


def make_dot_cluster_matr(alg_type):
    clusters = Cluster.objects(alg_type=alg_type)
    dots = Dot.objects
    clusters_len = clusters.count()
    dots_len = dots.count()
    zero_list = [0] * clusters_len
    w_matr = []
    dot_and_number_dict = {}
    cluster_and_number_dict = {clusters[i]:i  for i in range(clusters_len)}
    
    for i in range(dots_len):
        dot_and_number_dict[dots[i]] = i
        cluster = dots[i].cluster
        if cluster in cluster_and_number_dict:
            row = [0] * cluster_and_number_dict[cluster]
            row.append(1)
            row.extend([0]*(clusters_len - (i + 1)))
            w_matr.append(row)
    return w_matr


def get_output_num(session, self_ip):
    output_num = 0
    for item in session:
        if IP in item and item[IP].src == self_ip:
            output_num += 1
    return output_num

