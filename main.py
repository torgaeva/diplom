#!/usr/bin/env  python2.7

import sys
import getopt
import time

from clustering import *
from graphalg import graphalg


def usage():
    sys.stderr.write("{0}: usage: {0} -d <directory_with_session_files> -a <self_ip_addr>\n".format(os.path.basename(sys.argv[0])))
    sys.exit(1)


def main():
    connect("test")
    
    d = None
    a = None
    args, rest = getopt.getopt(sys.argv[1:], "d:a:")

    for key, val in args:
        if key == "-d":
            d = val
        elif key == "-a":
            a = val

    if d is None or a is None:
        usage()

    Dot.drop_collection()
    Cluster.drop_collection()

    # dots = make_test_dots()
    dots = make_dots(d, a)
    t = time.time()
    clusters = graphalg(5)
    print time.time() - t
    for cluster in clusters:
        cluster.show()
    print len(clusters)

    


if __name__ == "__main__":
    exit(main())
