from optics import *
# misc
from scapy.all import rdpcap
from scapy.error import Scapy_Exception
from mongoengine import connect

def get_Je(clusters_dots):
    for cluster_dots in clusters_dots:
        cluster_s = 0
        c_x_dict = get_dots_avg(cluster_dots)
        c_tmp_dot = Dot(session="", cluster=None, output_num=c_x_dict['output_num'], packet_count=c_x_dict['packet_count']).save()
        s_inside = 0
        for dot in cluster_dots:
            s_inside += get_distance_between(dot, c_tmp_dot)
        cluster_s += (s_inside ** 2)
        c_tmp_dot.delete()
    return cluster_s


def get_k_duda_hart(dots):
    '''two clusters'''
    dot_len = len(dots)
    je1 = get_Je([dots[:dot_len/2], dots[dot_len/2:]])
    je2 = get_Je([dots])
    return float(je2)/je1


def get_trace_B_and_W(clusters, x_dot):
    B_s = W_s = 0

    for cluster in clusters:
        cluster_dots = Dot.objects(cluster=cluster)
        c_x_dict = get_dots_avg(cluster_dots)
        c_tmp_dot = Dot(session="", cluster=None, output_num=c_x_dict['output_num'], packet_count=c_x_dict['packet_count']).save()

        # for trace B
        xc_distance = get_distance_between(x_dot, c_tmp_dot)
        B_s += len(cluster_dots) * (xc_distance ** 2)

        # for trace W
        W_s_inside = 0
        for dot in cluster_dots:
            W_s_inside += get_distance_between(dot, c_tmp_dot) ** 2
        W_s += W_s_inside

    c_tmp_dot.delete()

    return B_s, W_s


def get_CH_index(clusters, clusters_len, x_dot, dots_len):
    trace_B, trace_W = get_trace_B_and_W(clusters, x_dot)
    CH = float(trace_B/(clusters_len-1))/float(trace_W/(dots_len-clusters_len))
    return CH


def simple_dots_deviding_on_clusters(dots, k, dots_len):
    dots_count_in_clusters = dots_len/k
    clusters = [Cluster(alg_type=1).save() for i in range(k)]
    for index in range(dots_len):
        cluster_index = index/dots_count_in_clusters if index < k*dots_count_in_clusters else index%dots_count_in_clusters
        dots[index].update(set__cluster=clusters[cluster_index])
    return clusters


def get_k_CH():
    dots = Dot.objects
    dots_len = len(dots)
    CH_index_and_k_dict = {}
    x_dict = get_dots_avg(dots)
    x_dot = Dot(session="", cluster=None, output_num=x_dict['output_num'], packet_count=x_dict['packet_count']).save()

    for k in range(2, dots_len):
        clusters = simple_dots_deviding_on_clusters(dots, k, dots_len)
        CH_index_and_k_dict[k] = get_CH_index(clusters, k, x_dot, dots_len)
        for cluster in clusters:
            cluster.delete()

    x_dot.delete()
    return max(CH_index_and_k_dict, key=CH_index_and_k_dict.get)



def get_rule_of_thumb(dots_len):
    return math.sqrt(float(dots_len)/2)



def kmeans(k, maxiters):
    dots = Dot.objects
    dots_len = dots.count()
    set_dots_not_used(dots)
    clusters = simple_dots_deviding_on_clusters(dots, k, dots_len)
    set_cluster_center_real_dot(clusters)
    old_dict = dict((cluster, cluster.center_dot) for cluster in clusters)
    new_dict = {}
    iteration = 0
    while new_dict != old_dict and iteration < maxiters:
        iteration += 1
        for dot in dots[:]:
            min_dist = sys.maxint
            min_cluster = None
            for cluster in clusters:
                dist = get_distance_between(dot, cluster.center_dot)
                if dist < min_dist:
                    min_dist = dist
                    min_cluster = cluster
            dot.update(set__cluster=min_cluster)
        set_cluster_center_real_dot(clusters)
        new_dict = dict((cluster, cluster.center_dot) for cluster in clusters)
    return clusters


def set_dots_not_used(dots):
    dots.update(set__is_used=False)

def get_files_from_directory(directory):
    return [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]


def make_dots(directory, address):
    dots = []
    files_with_sessions = get_files_from_directory(directory)
    for session_file in files_with_sessions:
        try:
            session = rdpcap(directory + '/' + session_file)
            print session.__repr__()
            dot = Dot(session=session.__repr__(), cluster=None, output_num=get_output_num(session, address), packet_count=len(session))
            dots.append(dot)
        except Scapy_Exception as msg:
            print(msg)
    
    Dot.objects.insert(dots)
    return dots


def make_test_dots():
    dots = []
    try:
        session="test_session"
        dots.append(Dot(session=session, cluster=None, output_num=7, packet_count=5))
        dots.append(Dot(session=session, cluster=None, output_num=5, packet_count=6))
        dots.append(Dot(session=session, cluster=None, output_num=6, packet_count=6))
        dots.append(Dot(session=session, cluster=None, output_num=9, packet_count=10))
        dots.append(Dot(session=session, cluster=None, output_num=10, packet_count=9))
        dots.append(Dot(session=session, cluster=None, output_num=2, packet_count=4))
        dots.append(Dot(session=session, cluster=None, output_num=1, packet_count=3))
        dots.append(Dot(session=session, cluster=None, output_num=1, packet_count=1))
        dots.append(Dot(session=session, cluster=None, output_num=11, packet_count=21))

    except Scapy_Exception as msg:
        print(msg)

    Dot.objects.insert(dots)
    return dots


def get_all_dots_avg():
    dots = Dot.objects
    len_dots = dots.count()
    output_num = 0
    packet_count = 0
    for dot in dots:
        output_num += dot.output_num
        packet_count += dot.packet_count
    return {'output_num':float(output_num)/len_dots, 'packet_count':float(packet_count)/len_dots}



def get_dots_avg(dots):
    len_dots = len(dots)
    output_num = 0
    packet_count = 0
    for dot in dots:
        output_num += dot.output_num
        packet_count += dot.packet_count
    return {'output_num':float(output_num)/len_dots, 'packet_count':float(packet_count)/len_dots}



def set_cluster_center_real_dot(clusters):
    for cluster in clusters:
        cd = cluster.center_dot
        if cd:
            cd.update(set__center_of_cluster=False)
        c_dots = Dot.objects(cluster=cluster)
        c_avg_dot_dict = get_dots_avg(c_dots)
        c_avg_dot = Dot(session="a", cluster=None, output_num=c_avg_dot_dict['output_num'], packet_count=c_avg_dot_dict['packet_count']).save()
        min_dist = sys.maxint
        closest_dot = None
        for dot in c_dots:
            dist = get_distance_between(c_avg_dot, dot)
            if dist < min_dist:
                min_dist = dist
                closest_dot = dot
        closest_dot.update(set__cluster=cluster, set__center_of_cluster=True)
        c_avg_dot.delete()
