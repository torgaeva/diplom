#!/usr/bin/env python
import sys
from scapy.all import *
from classes import *
from multiprocessing import Process, Queue
from optics import OPTICS_live

q = Queue()
sessions = []

def run_sniff(iface):
    sniff(iface=iface, prn=pkt_callback, filter="ip", store=0)

def pkt_callback(pkt):
    q.put(str(pkt))


def read_queue(address):
    while not q.empty():
        pkt = q.get()
        pkt = Ether(pkt)
        if sessions:
            for session in sessions:
                if session['ip_src'] == pkt[IP].src and session['ip_dst'] == pkt[IP].dst and session['ip_sport'] == pkt[IP].sport and session['tcp_sport'] == pkt[TCP].sport:
                    print pkt[TCP].flags
                    if pkt[TCP].flags == 1 or pkt[TCP].flags == 4:
                        print "dot"
                        dot = Dot(session=session.__repr__(), cluster=None, output_num=get_output_num(session, address), packet_count=len(session))
                        sessions.remove(session)
                    else:
                        print "push"
                        session['packets'].append(pkt)
        else:
            session = {'ip_src':pkt[IP].src,
                       'ip_dst':pkt[IP].dst,
                       'ip_sport':pkt[IP].sport,
                       'tcp_sport':pkt[TCP].sport,
                       'packets':[pkt]}
            sessions.append(session)
def run_read_queue(address):
    while 1:
        read_queue(address)

def listen(iface, address):
    p_sniff = Process(target=run_sniff, args=(iface,))
    p_read = Process(target=run_read_queue, args=(address,))
    ep=5
    minpts=2
    ordered_dot_list = []
    p_opticslive = Process(target=OPTICS_live, args=(ep, minpts, ordered_dot_list,))
    p_sniff.start()
    p_read.start()
    p_opticslive.start()
    p_sniff.join()
    p_read.join()
    import time
    time.sleep(5)
    print ordered_dot_list


connect("test")
listen("wlan0", "10.22.47.1")